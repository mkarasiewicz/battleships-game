using System;
using System.Collections.Generic;
using Battleship.Game.Board;
using Xunit;

namespace Battleship.Game
{
    public class ShipShould
    {
        [Fact]
        public void CreateWhenAllFieldsInOneColumn()
        {
            //Given
            var coordinates = new List<Coordinate> {new Coordinate('A', 1), new Coordinate('B', 1)};

            //Then
            var ship = new Ship(coordinates);
        }

        [Fact]
        public void CreateWhenAllFieldsInOneRow()
        {
            //Given
            var coordinates = new List<Coordinate> {new Coordinate('A', 1), new Coordinate('A', 2)};

            //Then
            var ship = new Ship(coordinates);
        }

        [Fact]
        public void NotCreateWhenFieldsAreNotConsecutive()
        {
            //Given
            var coordinates = new List<Coordinate> {new Coordinate('A', 1), new Coordinate('C', 1)};

            //Then
            Assert.Throws<ArgumentException>(() => new Ship(coordinates));
        }

        [Fact]
        public void NotCreateWhenFieldsArePlacedDiagonally()
        {
            //Given
            var coordinates = new List<Coordinate> {new Coordinate('A', 1), new Coordinate('B', 2)};

            //Then
            Assert.Throws<ArgumentException>(() => new Ship(coordinates));
        }

        [Fact]
        public void NotCreateWhenFieldsOverlap()
        {
            //Given
            var coordinates = new List<Coordinate> {new Coordinate('A', 1), new Coordinate('A', 1)};

            //Then
            Assert.Throws<ArgumentException>(() => new Ship(coordinates));
        }
    }
}