using System.Collections.Generic;
using System.Linq;
using Battleship.Game.Board;
using Battleship.Game.OceanGridGenerator;
using Moq;
using Xunit;

namespace Battleship.Game
{
    public class ShipsPositionerShould
    {
        private readonly List<Coordinate> _availableBoardCoordinates;
        private readonly BoardSize _boardSize;
        private readonly Mock<IShipAnchorGenerator> _shipAnchorGeneratorMock;
        private readonly Mock<IShipOrientationGenerator> _shipOrientationGeneratorMock;
        private readonly List<ShipToPlace> _shipsToPlace;

        public ShipsPositionerShould()
        {
            _boardSize = new BoardSize(10, 10);
            _availableBoardCoordinates = BoardGenerator.GenerateBoardCoordinateGrid(_boardSize);
            _shipsToPlace = new List<ShipToPlace> {new ShipToPlace(5), new ShipToPlace(4), new ShipToPlace(4)};

            _shipAnchorGeneratorMock = new Mock<IShipAnchorGenerator>();
            _shipOrientationGeneratorMock = new Mock<IShipOrientationGenerator>();
        }

        [Fact]
        public void PlaceShipOnABoard()
        {
            //Given
            var shipPositioner = new ShipsPositioner(_boardSize);

            //When
            var ships = shipPositioner.PlaceShips(_shipsToPlace);

            //Then
            Assert.Equal(_shipsToPlace.Count, ships.Count);
        }

        [Fact]
        public void PlacedShipsOnABoardShouldNotOverlap()
        {
            //Given
            var shipPositioner = new ShipsPositioner(_boardSize);
            var shipsToPlaceFieldsCount = _shipsToPlace.Aggregate(0, (count, s) => count + s.Size);

            //When
            var ships = shipPositioner.PlaceShips(_shipsToPlace);
            var shipsFieldsCount = ships.Aggregate(0, (count, s) => count + s.Size);

            //Then
            Assert.Equal(shipsToPlaceFieldsCount, shipsFieldsCount);
        }

        [Fact]
        public void PlacedShipOnABoardShouldBeInBoardBound()
        {
            //Given
            var notFitRowShipToPlace = (ShipOrientation.Horizontal, new Coordinate('J', 1));
            var notFitColumnShipToPlace = (ShipOrientation.Vertical, new Coordinate('C', 10));
            var validShipToPlace1 = (ShipOrientation.Horizontal, new Coordinate('A', 1));
            var validShipToPlace2 = (ShipOrientation.Horizontal, new Coordinate('B', 2));
            var validShipToPlace3 = (ShipOrientation.Vertical, new Coordinate('C', 3));

            var shipsToPlaceOnBoard = new List<(ShipOrientation, Coordinate)>
            {
                notFitRowShipToPlace,
                notFitColumnShipToPlace,
                validShipToPlace1,
                validShipToPlace2,
                validShipToPlace3
            };

            PositionerWillTryToPlace(shipsToPlaceOnBoard);

            var shipPositioner = new ShipsPositioner(
                _availableBoardCoordinates,
                _shipOrientationGeneratorMock.Object,
                _shipAnchorGeneratorMock.Object);

            //When
            var ships = shipPositioner.PlaceShips(_shipsToPlace);

            //Then
            Assert.DoesNotContain(ships, ship => ship.IsPlacedAt(notFitRowShipToPlace.Item2));
            Assert.DoesNotContain(ships, ship => ship.IsPlacedAt(notFitColumnShipToPlace.Item2));
        }

        private void PositionerWillTryToPlace(List<(ShipOrientation, Coordinate)> shipsToPlaceOnBoard)
        {
            var generateOrientationSequence =
                _shipOrientationGeneratorMock.SetupSequence(s => s.GenerateShipOrientation());
            var generateAnchorSequence =
                _shipAnchorGeneratorMock.SetupSequence(s => s.GenerateShipAnchor(_availableBoardCoordinates));

            shipsToPlaceOnBoard.ForEach(
                shipToPlace =>
                {
                    var (shipOrientation, coordinate) = shipToPlace;
                    generateOrientationSequence.Returns(shipOrientation);
                    generateAnchorSequence.Returns(coordinate);
                });
        }
    }
}
