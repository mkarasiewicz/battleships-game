using System.Collections.Generic;
using Battleship.Game.Board;
using Battleship.Game.OceanGridGenerator;
using Moq;
using Xunit;

namespace Battleship.Game
{
    public class BattleshipGameShould
    {
        private readonly BoardSize _boardSize;
        private readonly List<Ship> _shipsList;
        private readonly List<ShipToPlace> _shipsToPlace;

        public BattleshipGameShould()
        {
            _boardSize = new BoardSize(10, 10);
            _shipsToPlace = new List<ShipToPlace> {new ShipToPlace(5)};
            _shipsList = new List<Ship>
            {
                new Ship(
                    new List<Coordinate>
                    {
                        new Coordinate('A', 1),
                        new Coordinate('A', 2),
                        new Coordinate('A', 3),
                        new Coordinate('A', 4),
                        new Coordinate('A', 5)
                    })
            };
        }

        private Mock<IShipsPositioner> ShipPositionerWillPlaceGivenShipsToOnBoard(
            List<ShipToPlace> shipsToPlace,
            List<Ship> shipsList)
        {
            var shipPositionerMock = new Mock<IShipsPositioner>();
            shipPositionerMock.Setup(sp => sp.PlaceShips(shipsToPlace)).Returns(shipsList);

            return shipPositionerMock;
        }

        [Fact]
        public void HitWhenShipOnAField()
        {
            //Given
            var shipPositionerMock = ShipPositionerWillPlaceGivenShipsToOnBoard(_shipsToPlace, _shipsList);

            var game = new BattleshipGame(_boardSize, _shipsToPlace) {ShipsPositioner = shipPositionerMock.Object};

            //When
            var moveResult = game.MakeAMove(new Move('A', 1));

            //Then
            Assert.IsType<Hit>(moveResult);
        }

        [Fact]
        public void MissWhenNoShipInField()
        {
            //Given
            var game = new BattleshipGame(_boardSize, _shipsToPlace);

            //When
            var moveResult = game.MakeAMove(new Move('J', 10));

            //Then
            Assert.IsType<Miss>(moveResult);
        }

        [Fact]
        public void NotMakeAMoveWhenCoordinatesOutOfBoard()
        {
            //Given
            var game = new BattleshipGame(_boardSize, _shipsToPlace);

            //When
            var moveResult = game.MakeAMove(new Move('Z', 20));

            //Then
            Assert.IsType<InvalidMove>(moveResult);
        }

        [Fact]
        public void WinWhenAllShipsSunk()
        {
            //Given
            var shipPositionerMock = ShipPositionerWillPlaceGivenShipsToOnBoard(_shipsToPlace, _shipsList);

            var game = new BattleshipGame(_boardSize, _shipsToPlace) {ShipsPositioner = shipPositionerMock.Object};

            //When
            game.MakeAMove(new Move('A', 1));
            game.MakeAMove(new Move('A', 2));
            game.MakeAMove(new Move('A', 3));
            game.MakeAMove(new Move('A', 4));
            var moveResult = game.MakeAMove(new Move('A', 5));

            //Then
            Assert.IsType<Win>(moveResult);
        }
    }
}