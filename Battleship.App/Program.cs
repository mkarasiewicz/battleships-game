using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Battleship.App.View;
using Battleship.Game;

namespace Battleship.App
{
    internal static class Program
    {
        private const string CoordinatesRegexp = "^([A-Z]|[a-z])(\\d+)$";

        public static void Main(string[] args)
        {
            var boardSize = new BoardSize(10, 10);
            var ships = new List<ShipToPlace> {new ShipToPlace(5), new ShipToPlace(4), new ShipToPlace(4)};
            var battleshipGame = new BattleshipGame(boardSize, ships);
            var boardView = new BoardView(boardSize);

            Console.WriteLine($"Welcome to Battleship Game. Ships to shoot down for today are: {string.Join(",", ships)}");

            DrawTheBoard(boardView, battleshipGame.MovesHistory);

            RunGameLoop(battleshipGame, boardView, boardSize);

            Console.WriteLine("THE END, thanks!");
        }

        private static void RunGameLoop(BattleshipGame battleshipGame, BoardView boardView, BoardSize boardSize)
        {
            while (!battleshipGame.IsUserWon)
            {
                Console.Write("Enter Coordinates : ");
                var moveInput = Console.ReadLine();
                if (IsItValidInput(moveInput))
                {
                    var (column, row) = GetInputCoordinates(moveInput);
                    var moveResult = battleshipGame.MakeAMove(new Move(column, row));

                    Console.WriteLine(moveResult.Message);

                    DrawTheBoard(boardView, battleshipGame.MovesHistory);
                }
                else
                {
                    Console.WriteLine($"Enter coordinates from A1 to {boardSize.LastColumnAsChar}{boardSize.RowsNumber}");
                }
            }
        }

        private static void DrawTheBoard(BoardView boardView, MovesHistory battleshipGameMovesHistory)
        {
            Console.Write(boardView.Draw(battleshipGameMovesHistory));
        }

        private static bool IsItValidInput(string moveInput)
        {
            return Regex.IsMatch(moveInput, CoordinatesRegexp);
        }

        private static (char column, int row) GetInputCoordinates(string moveInput)
        {
            var input = Regex.Split(moveInput, CoordinatesRegexp).Where(s => s != string.Empty).ToList();
            var column = input[0].ToUpper().ToCharArray()[0];
            var row = int.Parse(input[1]);

            return (column, row);
        }
    }
}
