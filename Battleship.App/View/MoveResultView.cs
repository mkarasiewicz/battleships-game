using Battleship.Game;

namespace Battleship.App.View
{
    internal class MoveResultView
    {
        private readonly MoveResult _moveResult;

        public MoveResultView(MoveResult moveResult)
        {
            _moveResult = moveResult;
        }

        public override string ToString()
        {
            var field = _moveResult switch
            {
                Hit _ => "x",
                Miss _ => "*",
                _ => " "
            };

            return $"|{field}";
        }
    }
}
