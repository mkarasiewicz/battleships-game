using System.Text;
using Battleship.Game;

namespace Battleship.App.View
{
    internal class BoardView
    {
        private readonly BoardSize _boardSize;

        public BoardView(BoardSize boardSize)
        {
            _boardSize = boardSize;
        }

        public string Draw(MovesHistory battleshipGameMovesHistory)
        {
            var sb = new StringBuilder();

            DrawColumnsHeader(sb);

            DrawRows(battleshipGameMovesHistory, sb);

            return sb.ToString();
        }

        private void DrawColumnsHeader(StringBuilder sb)
        {
            var columnHeaderPadding = _boardSize.RowsNumber > 9 ? "   " : "  ";
            sb.Append(columnHeaderPadding);

            for (var column = 'A'; column < 'A' + _boardSize.ColumnsNumber; column++)
            {
                sb.Append($" {column}");
            }

            sb.AppendLine("");
        }

        private void DrawRows(MovesHistory battleshipGameMovesHistory, StringBuilder sb)
        {
            for (var row = 1; row <= _boardSize.RowsNumber; row++)
            {
                var rowSpace = _boardSize.RowsNumber > 9 && row < 10 ? "  " : " ";
                sb.Append(row + rowSpace);
                DrawField(battleshipGameMovesHistory, row, sb);

                sb.AppendLine("|");
            }
        }

        private void DrawField(MovesHistory battleshipGameMovesHistory, int row, StringBuilder sb)
        {
            for (var column = 'A'; column < 'A' + _boardSize.ColumnsNumber; column++)
            {
                var moveResult = battleshipGameMovesHistory.GetMoveResultByMove(new Move(column, row));
                sb.Append(new MoveResultView(moveResult));
            }
        }
    }
}
