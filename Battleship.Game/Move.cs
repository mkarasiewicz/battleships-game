using System;

namespace Battleship.Game
{
    public readonly struct Move : IEquatable<Move>
    {
        public char Column { get; }
        public int Row { get; }

        public Move(char column, int row)
        {
            Column = column;
            Row = row;
        }

        public static bool operator ==(Move move1, Move move2)
        {
            return move1.Equals(move2);
        }

        public static bool operator !=(Move move1, Move move2)
        {
            return !move1.Equals(move2);
        }

        public bool Equals(Move coordinate)
        {
            return Column == coordinate.Column && Row == coordinate.Row;
        }

        public override bool Equals(object obj)
        {
            return obj is Move other && Equals(other);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Column, Row);
        }

        public override string ToString()
        {
            return $"{Column}{Row.ToString()}";
        }
    }
}
