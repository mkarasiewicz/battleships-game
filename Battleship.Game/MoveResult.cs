namespace Battleship.Game
{
    public abstract class MoveResult
    {
        internal MoveResult(Move move)
        {
            MadeMove = move;
        }

        public Move MadeMove { get; }
        public abstract string Message { get; }
    }

    public class Miss : MoveResult
    {
        public Miss(Move move) : base(move)
        {
        }

        public override string Message => "Miss...";
    }

    public class Hit : MoveResult
    {
        public Hit(Move move) : base(move)
        {
        }

        public override string Message => "You HIT the ship!";
    }

    public class InvalidMove : MoveResult
    {
        public InvalidMove(Move move) : base(move)
        {
        }

        public override string Message => "Sorry, invalid move.";
    }

    public class Win : MoveResult
    {
        public Win(Move move) : base(move)
        {
        }

        public override string Message => "Congratulations Commander! You have destroyed the entire enemy fleet.";
    }
}
