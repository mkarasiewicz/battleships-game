namespace Battleship.Game
{
    public readonly struct BoardSize
    {
        public BoardSize(int columnsNumber, int rowsNumber)
        {
            ColumnsNumber = columnsNumber;
            RowsNumber = rowsNumber;
        }

        public int RowsNumber { get; }
        public int ColumnsNumber { get; }

        public int LastColumnAsCharInt => ColumnsNumber + 'A';
        public char LastColumnAsChar => (char) (LastColumnAsCharInt - 1);
    }
}
