using System.Collections.Generic;
using System.Linq;
using Battleship.Game.Board;
using Battleship.Game.OceanGridGenerator;

namespace Battleship.Game
{
    public class BattleshipGame
    {
        private readonly BoardSize _boardSize;
        private readonly List<ShipToPlace> _shipsToPlace;
        private List<Ship> _ships;

        public bool IsUserWon => MovesHistory.LastMove is Win;

        public MovesHistory MovesHistory { get; } = new MovesHistory();

        internal IShipsPositioner ShipsPositioner { get; set; }

        private IEnumerable<Ship> Ships
        {
            get { return _ships ??= ShipsPositioner.PlaceShips(_shipsToPlace); }
        }

        public BattleshipGame(BoardSize boardSize, List<ShipToPlace> shipsToPlace)
        {
            _boardSize = boardSize;
            _shipsToPlace = shipsToPlace;
            ShipsPositioner = new ShipsPositioner(boardSize);
        }

        public MoveResult MakeAMove(Move move)
        {
            MoveResult moveResult;

            if (!IsMoveInBoardBound(move) || IsUserWon)
            {
                moveResult = new InvalidMove(move);
            }
            else
            {
                if (IsAHit(move))
                {
                    if (AreAllShipsSunk())
                    {
                        moveResult = new Win(move);
                    }
                    else
                    {
                        moveResult = new Hit(move);
                    }
                }
                else
                {
                    moveResult = new Miss(move);
                }
            }

            MovesHistory.AddMove(moveResult);

            return moveResult;
        }

        private bool AreAllShipsSunk()
        {
            return Ships.All(ship => ship.IsSunk());
        }

        private bool IsAHit(Move move)
        {
            return Ships.Any(ship => ship.IsAHit(Coordinate.FromMove(move)));
        }

        private bool IsMoveInBoardBound(Move move)
        {
            return move.Column < _boardSize.LastColumnAsCharInt && move.Row <= _boardSize.RowsNumber;
        }
    }
}
