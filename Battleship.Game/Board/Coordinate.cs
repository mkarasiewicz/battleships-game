using System;

namespace Battleship.Game.Board
{
    internal readonly struct Coordinate : IEquatable<Coordinate>
    {
        public Coordinate(char column, int row)
        {
            Column = column;
            Row = row;

            if (!IsColumnInAllowedRange(column) || !IsRowInAllowedRange(row))
            {
                throw new ArgumentException("Invalid coordinate");
            }
        }

        public int Row { get; }

        public char Column { get; }

        public bool Equals(Coordinate coordinate)
        {
            return coordinate.Row == Row && coordinate.Column == Column;
        }

        public override string ToString()
        {
            return $"{Column}{Row.ToString()}";
        }

        public static Coordinate FromMove(Move move)
        {
            return new Coordinate(move.Column, move.Row);
        }

        private bool IsColumnInAllowedRange(char column)
        {
            return column >= 'A' && column <= 'Z';
        }

        private bool IsRowInAllowedRange(int row)
        {
            return row >= 1 && row <= 25;
        }
    }
}
