using System;
using System.Collections.Generic;
using System.Linq;

namespace Battleship.Game.Board
{
    internal enum ShipOrientation
    {
        Vertical,
        Horizontal
    }

    internal readonly struct Ship
    {
        private readonly List<Coordinate> _coordinates;
        private readonly List<Coordinate> _hits;

        public int Size => _coordinates.Count;

        public Ship(List<Coordinate> coordinates)
        {
            _coordinates = coordinates;
            _hits = new List<Coordinate>();

            ValidateShipOrigin(coordinates);

            ValidateShipFieldsAreNotOverlapping(coordinates);

            ValidateShipFieldsAreConsecutive(coordinates);
        }

        public bool IsAHit(Coordinate coordinate)
        {
            var isAHit = _coordinates.Contains(coordinate);
            if (isAHit)
            {
                _hits.Add(coordinate);
            }

            return isAHit;
        }

        public bool IsPlacedAt(Coordinate coordinate)
        {
            return _coordinates.Contains(coordinate);
        }

        public bool IsSunk()
        {
            return _hits.Count == _coordinates.Count;
        }

        private static void ValidateShipOrigin(IReadOnlyList<Coordinate> coordinates)
        {
            var firstCell = coordinates[0];
            if (coordinates.Any(c => c.Column != firstCell.Column))
            {
                if (coordinates.Any(c => c.Row != firstCell.Row))
                {
                    throw new ArgumentException("Ship placed diagonally.");
                }
            }
        }

        private static void ValidateShipFieldsAreNotOverlapping(List<Coordinate> coordinates)
        {
            if (coordinates.Distinct().Count() < coordinates.Count)
            {
                throw new ArgumentException("Ship fields overlaps");
            }
        }

        private static void ValidateShipFieldsAreConsecutive(List<Coordinate> coordinates)
        {
            var shipOrientation = GetShipOrientation(coordinates);


            var isConsecutive = shipOrientation switch
            {
                ShipOrientation.Vertical => AreSelectedFieldsConsecutiveInRow(coordinates),
                ShipOrientation.Horizontal => AreSelectedFieldsConsecutiveInColumn(coordinates),
                _ => false
            };

            if (!isConsecutive)
            {
                throw new ArgumentException("Ship fields are not consecutive");
            }
        }

        private static bool AreSelectedFieldsConsecutiveInRow(List<Coordinate> shipCoordinates)
        {
            return IsConsecutive((coordinate, j) => coordinate.Row - j, shipCoordinates);
        }

        private static bool AreSelectedFieldsConsecutiveInColumn(List<Coordinate> shipCoordinates)
        {
            return IsConsecutive((coordinate, j) => coordinate.Column - j, shipCoordinates);
        }

        private static bool IsConsecutive(Func<Coordinate, int, int> selector, IEnumerable<Coordinate> shipCoordinates)
        {
            return !shipCoordinates.Select(selector).Distinct().Skip(1).Any();
        }

        private static ShipOrientation GetShipOrientation(List<Coordinate> coordinates)
        {
            var firstField = coordinates[0];
            return coordinates.All(c => c.Column == firstField.Column)
                ? ShipOrientation.Vertical
                : ShipOrientation.Horizontal;
        }
    }
}