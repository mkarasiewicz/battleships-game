using System.Collections.Generic;

namespace Battleship.Game.Board
{
    internal static class BoardGenerator
    {
        internal static List<Coordinate> GenerateBoardCoordinateGrid(BoardSize boardSize)
        {
            var coordinates = new List<Coordinate>();
            for (var column = 'A'; column < 'A' + boardSize.ColumnsNumber; column++)
            {
                for (var row = 1; row <= boardSize.RowsNumber; row++)
                {
                    coordinates.Add(new Coordinate(column, row));
                }
            }

            return coordinates;
        }
    }
}
