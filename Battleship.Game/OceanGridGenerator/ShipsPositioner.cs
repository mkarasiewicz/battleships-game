using System;
using System.Collections.Generic;
using System.Linq;
using Battleship.Game.Board;

namespace Battleship.Game.OceanGridGenerator
{
    internal class ShipsPositioner : IShipsPositioner
    {
        private readonly List<Coordinate> _availableBoardCoordinates;
        private readonly IShipAnchorGenerator _shipAnchorGenerator;
        private readonly IShipOrientationGenerator _shipOrientationGenerator;

        public ShipsPositioner(BoardSize boardSize)
        {
            _availableBoardCoordinates = BoardGenerator.GenerateBoardCoordinateGrid(boardSize);
            _shipOrientationGenerator = new ShipOrientationGenerator();
            _shipAnchorGenerator = new ShipAnchorGenerator();
        }

        public ShipsPositioner(
            List<Coordinate> availableBoardCoordinates,
            IShipOrientationGenerator shipOrientationGenerator,
            IShipAnchorGenerator shipAnchorGenerator)
        {
            _availableBoardCoordinates = availableBoardCoordinates;
            _shipOrientationGenerator = shipOrientationGenerator;
            _shipAnchorGenerator = shipAnchorGenerator;
        }

        public List<Ship> PlaceShips(IEnumerable<ShipToPlace> shipsToPlace)
        {
            return shipsToPlace.Select(BuildShip).ToList();
        }

        private Ship BuildShip(ShipToPlace shipToPlace)
        {
            var shipCoordinates = new List<Coordinate>();

            while (shipCoordinates.Count != shipToPlace.Size)
            {
                var orientation = _shipOrientationGenerator.GenerateShipOrientation();
                var anchor = _shipAnchorGenerator.GenerateShipAnchor(_availableBoardCoordinates);
                shipCoordinates = orientation switch
                {
                    ShipOrientation.Vertical => GetVerticalCoordinatesFromAnchor(anchor, shipToPlace),
                    ShipOrientation.Horizontal => GetHorizontalCoordinatesFromAnchor(anchor, shipToPlace),
                    _ => new List<Coordinate>()
                };
            }

            return new Ship(shipCoordinates);
        }


        private List<Coordinate> GetHorizontalCoordinatesFromAnchor(Coordinate anchor, ShipToPlace shipToPlace)
        {
            var coordinatesInRow = _availableBoardCoordinates.Where(c => c.Row == anchor.Row).ToList();

            return GetCoordinatesFromAnchor(anchor, shipToPlace, coordinatesInRow);
        }

        private List<Coordinate> GetVerticalCoordinatesFromAnchor(Coordinate anchor, ShipToPlace shipToPlace)
        {
            var coordinatesInColumn = _availableBoardCoordinates.Where(c => c.Column == anchor.Column).ToList();

            return GetCoordinatesFromAnchor(anchor, shipToPlace, coordinatesInColumn);
        }

        private List<Coordinate> GetCoordinatesFromAnchor(
            Coordinate anchor,
            ShipToPlace shipToPlace,
            List<Coordinate> coordinatesInLine)
        {
            var positionOfAnchorInLine = coordinatesInLine.IndexOf(anchor);
            if (IsShipFitTheLine(shipToPlace.Size, coordinatesInLine.Count, positionOfAnchorInLine))
            {
                var shipCoordinates = coordinatesInLine.GetRange(positionOfAnchorInLine, shipToPlace.Size);

                if (AreShipCoordinatesValid(shipCoordinates))
                {
                    shipCoordinates.ForEach(sc => _availableBoardCoordinates.Remove(sc));
                    return shipCoordinates;
                }
            }

            return new List<Coordinate>();
        }

        private bool AreShipCoordinatesValid(List<Coordinate> shipCoordinates)
        {
            try
            {
                new Ship(shipCoordinates);

                return true;
            }
            catch
            {
                return false;
            }
        }

        private static bool IsShipFitTheLine(
            int shipToPlaceSize,
            int coordinatesInLineCount,
            int positionOfAnchorInLine)
        {
            if (positionOfAnchorInLine >= 0)
            {
                return positionOfAnchorInLine + shipToPlaceSize < coordinatesInLineCount - 1;
            }

            return false;
        }
    }
}
