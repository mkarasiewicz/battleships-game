using System;
using System.Collections.Generic;
using Battleship.Game.Board;

namespace Battleship.Game.OceanGridGenerator
{
    internal interface IShipAnchorGenerator
    {
        Coordinate GenerateShipAnchor(IList<Coordinate> availableBoardCoordinates);
    }

    internal class ShipAnchorGenerator : IShipAnchorGenerator
    {
        public Coordinate GenerateShipAnchor(IList<Coordinate> availableBoardCoordinates)
        {
            var random = new Random();
            var randomNumber = random.Next(0, availableBoardCoordinates.Count);
            var coordinate = availableBoardCoordinates[randomNumber];

            return coordinate;
        }
    }
}