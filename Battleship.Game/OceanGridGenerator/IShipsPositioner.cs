using System.Collections.Generic;
using Battleship.Game.Board;

namespace Battleship.Game.OceanGridGenerator
{
    internal interface IShipsPositioner
    {
        List<Ship> PlaceShips(IEnumerable<ShipToPlace> shipsToPlace);
    }
}