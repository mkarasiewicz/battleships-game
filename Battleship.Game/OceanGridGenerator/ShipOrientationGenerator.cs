using System;
using Battleship.Game.Board;

namespace Battleship.Game.OceanGridGenerator
{
    internal interface IShipOrientationGenerator
    {
        ShipOrientation GenerateShipOrientation();
    }

    internal class ShipOrientationGenerator : IShipOrientationGenerator
    {
        public ShipOrientation GenerateShipOrientation()
        {
            var values = Enum.GetValues(typeof(ShipOrientation));
            var random = new Random();
            return (ShipOrientation) values.GetValue(random.Next(values.Length));
        }
    }
}