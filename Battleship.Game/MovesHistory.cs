using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Battleship.Game
{
    public class MovesHistory
    {
        private readonly IList<MoveResult> _moves = new List<MoveResult>();

        public MoveResult LastMove => _moves.LastOrDefault();

        public void AddMove(MoveResult coordinate)
        {
            _moves.Add(coordinate);
        }

        public MoveResult GetMoveResultByMove(Move move)
        {
            return _moves.FirstOrDefault(moveResult => moveResult.MadeMove == move);
        }

        public IReadOnlyCollection<MoveResult> GetMoves()
        {
            return new ReadOnlyCollection<MoveResult>(_moves);
        }
    }
}