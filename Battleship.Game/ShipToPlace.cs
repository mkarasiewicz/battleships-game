using System;
using System.Linq;

namespace Battleship.Game
{
    public readonly struct ShipToPlace
    {
        public int Size { get; }

        private static readonly int[] AllowedSizes = {2, 3, 4, 5};

        public ShipToPlace(int size)
        {
            if (!AllowedSizes.Contains(size))
            {
                throw new ArgumentException("Not allowed ship size.");
            }

            Size = size;
        }

        public override string ToString()
        {
            return Size.ToString();
        }
    }
}