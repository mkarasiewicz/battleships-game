﻿# Battleships game
One-sided game of Battleships against ships placed by the computer.

Reqirementes:

* dotnet sdk 3.1.0

To run the game: 

`$ cd Battleship.App && dotnet run`

To run the test:

`$ dotnet test`
